from __future__ import print_function
import sys

def eprint(*args, **kwargs):
    '''Ispisuje na stderr umjesto stdin.'''
    print(*args, file=sys.stderr, **kwargs)

def stvori_prijelaze():
    '''Stvara dictionary za f-ju prijelaza.'''
    dicc = dict()

    for line in sys.stdin:
        line = line[:-1]
        dijelovi = line.split("->")
        kljuc = dijelovi[0].split(",")
        nova = dijelovi[1].split(",")

        kljuc_pravi = (kljuc[0], kljuc[1])
        skok = (nova[0], nova[1], nova[2])
        dicc[kljuc_pravi] = skok

    return dicc

class SimTS:
    '''Razred simulira Turingov stroj.'''
    __Q = tuple()
    __sigma = tuple()
    __gamma = tuple()
    __prazni = None
    __traka = None
    __prihvatljiva = None
    __pocetno = None
    __pocglava = None
    __delta = None
    __trenutno = None
    __indeks = None
    __kraj = None
    __min = 0
    __max = 69

    def __init__(self):
        self.__Q = tuple(input().split(","))         #skup stanja
        self.__sigma = tuple(input().split(","))     #abeceda
        self.__gamma = tuple(input().split(","))     #znakovi trake
        self.__prazni = input()                      #znak prazne celije
        self.__traka = list(input())                 #zapis trake
        self.__prihvatljiva = tuple(input().split(","))  #skup F
        self.__pocetno = input()                     #pocetno stanje
        self.__pocglava = input()                    #pocetak glave TS-a
        self.__delta = stvori_prijelaze()            #f-ja prijelaza
        self.__trenutno = self.__pocetno             #trenutno stanje TS-a
        self.__indeks = int(self.__pocglava)
        self.__kraj = False
        self.kontrola_elemenata()

    def prijedi(self):
        '''Mijenja stanja ovisno o funkciji prijelaza
        i trenutnom stanju i znaku na traci.'''
        kljuc = (self.__trenutno, self.__traka[self.__indeks])

        try:
            novo = self.__delta[kljuc]
        except:
            self.__kraj = True
            return False

        if novo[2] is "L":
            if self.__indeks - 1 < self.__min:
                self.__kraj = True
                return False
            self.__traka[self.__indeks] = novo[1]
            self.__indeks -= 1
        elif novo[2] is "R":
            if self.__indeks + 1 > self.__max:
                self.__kraj = True
                return False
            self.__traka[self.__indeks] = novo[1]
            self.__indeks += 1

        self.__trenutno = novo[0]
        if self.__trenutno in self.__prihvatljiva:
            self.__kraj = True
            return False
        return True

    def obradi(self):
        '''Zapocinje simulaciju TS-a.'''
        while self.__kraj is False:
            self.prijedi()
            self.kontrolni_ispis()

    def ispis(self):
        '''Ispisuje sve nakon obrade.'''
        niz = self.__trenutno + "|" + str(self.__indeks) + "|"
        for znak in self.__traka:
            niz += znak

        if self.__trenutno in self.__prihvatljiva:
            niz += "|1"
        else:
            niz += "|0"

        print(niz)
        return

    def kontrola_elemenata(self):
        eprint("Stanja: " + str(self.__Q))
        eprint("Sigma: " + str(self.__sigma))
        eprint("Gamma: " + str(self.__gamma))
        eprint("Prazni: " + str(self.__prazni))
        eprint("Traka: " + str(self.__traka))
        eprint("Prihvatljiva: " + str(self.__prihvatljiva))
        eprint("Pocetno: " + str(self.__pocetno))
        eprint("Pocglava: " + str(self.__pocglava))
        eprint("Delta: " + str(self.__delta))
        eprint("Trenutno: " + str(self.__trenutno))
        eprint("Indeks: " + str(self.__indeks))
        eprint("Kraj: " + str(self.__kraj))

    def kontrolni_ispis(self):
        '''Ispisuje sve nakon obrade.'''
        niz = self.__trenutno + "|" + str(self.__indeks) + "|"
        for znak in self.__traka:
            niz += znak

        if self.__trenutno in self.__prihvatljiva:
            niz += "|1"
        else:
            niz += "|0"

        eprint(niz)
        return

stroj = SimTS()
stroj.obradi()
stroj.ispis()
