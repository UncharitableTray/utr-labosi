from __future__ import print_function
import sys

def eprint(*args, **kwargs):
    '''Ispisuje na stderr umjesto stdin.'''

    print(*args, file=sys.stderr, **kwargs)

class Parser:
    '''Parsira nizove za jezik labosa...'''
    __znakovi = []
    __string = None
    __lang = None

    def __init__(self):
        self.__znakovi = list(input().strip())
        self.__string = ""
        self.__lang = list("a")
        self.__lang.append("b")
        self.__lang.append("c")
        self.__lang = tuple(self.__lang)

    def peek(self):
        '''Vraca vrijednost sljedeceg znaka'''
        try:
            return self.__znakovi[0]
        except:
            return ""

    def pomak(self):
        '''Pomice znakove za 1...'''
        try:
            self.__znakovi.pop(0)
            return True
        except:
            return False

    def pocni(self):
        '''Zapocinje parsiranje...'''

        if self.peek() not in self.__lang:
            return False
        done = self.S()

        print(self.__string)
        self.pomak()
        if done and not self.__znakovi:
            print("DA")
        else:
            print("NE")

    def S(self):
        '''Metoda za produkciju S.'''
        self.__string += "S"

        if self.peek() is "a":
            self.pomak()
            return self.A() and self.B()
        elif self.peek() is "b":
            self.pomak()
            return self.B() and self.A()
        return False

    def A(self):
        self.__string += "A"

        if self.peek() is "a":
            self.pomak()
            return True
        elif self.peek() is "b":
            self.pomak()
            return self.C()
        return False

    def B(self):
        self.__string += "B"

        if self.peek() is not "c":
            return True

        self.pomak()
        if self.peek() is not "c":
            return False

        self.pomak()
        if self.S() is False:
            return False

        if self.peek() is not "b":
            return False

        self.pomak()
        if self.peek() is not "c":
            return False

        self.pomak()
        return True

    def C(self):
        '''Metoda za C produkciju.'''

        self.__string += "C"
        return self.A() and self.A()

parser = Parser()
parser.pocni()