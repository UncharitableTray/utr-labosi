from __future__ import print_function
import sys
from eNFA import eNFA

EPSILON = "$"
EMPTY_SET = "#"

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def zapocni():
    ulazni_nizovi = obradi_nizove(input())
    stanja = obradi(input(), ",")
    abeceda = obradi(input(), ",")
    prihvatljiva = obradi(input(), ",")
    pocetno = input()

    prijelazi = []
    for line in sys.stdin:
        prijelazi.append(line)
    abeceda.append(EPSILON)

    amen = eNFA(ulazni_nizovi, stanja, abeceda, prijelazi, prihvatljiva, pocetno)
    return amen

def obradi_nizove(string):
    velika_lista = string.split("|")
    konacna_lista = []

    for lista in velika_lista:
        mini_lista = lista.split(",")
        konacna_lista.insert(len(konacna_lista), mini_lista)

    print("Konacna lista je:")
    print(konacna_lista)
    return konacna_lista

def obradi(string, delimiter):
    lista = string.split(delimiter)
    return lista

def iteriraj_po_epsilon(nfa, pocetna_stanja):
    nova_stanja = set()         #skup stanja nakon e-tranzicije
    broj_novih_stanja = 0

    for stanje in pocetna_stanja:
        broj_novih_stanja += 1      #broj stanja prije e-prijelaza
        nova_stanja.add(stanje)     #inicijaliziraj skup stanja nakon e-tranzicije

    while True:                     #dok ima promjene u skupu trenutnih, prelazi preko e

        broj_stanja_skup_nakon = broj_novih_stanja

        lista_stanja_nakon_epsilona = []      #lista novih stanja nakon e-prijelaza

        for stanje in nova_stanja:      #iteriraj po trenutnim stanjima i puni listu novima
            kljuc = stanje + "," + EPSILON
            prijelaz = nfa.prijelazi.get(kljuc)
            if prijelaz != None:
                lista_stanja_nakon_epsilona.extend(prijelaz)

        #iteriraj po svim stanjima u listi
        #ako stanje ne postoji u skupu, dodaj ga i povecaj brojac
        if lista_stanja_nakon_epsilona != None:
            for novo in lista_stanja_nakon_epsilona:
                if novo != None and novo != EMPTY_SET and novo not in nova_stanja:
                    nova_stanja.add(novo)       #dodaj to stanje
                    broj_stanja_skup_nakon += 1 #povecaj brojac
                #else ne radi nista

        if broj_stanja_skup_nakon == broj_novih_stanja:
            break

    return nova_stanja

def mijenjaj_stanja(nfa, trenutna_stanja, znak):
    nova_stanja = set()

    for stanje in trenutna_stanja:
        kljuc = stanje + "," + znak

        niz_stanja_nakon_promjene = nfa.prijelazi.get(kljuc)

        if niz_stanja_nakon_promjene != None:
            for novo in niz_stanja_nakon_promjene:
                if novo != EMPTY_SET:
                    nova_stanja.add(novo)

    return nova_stanja

def ispis_stanja(trenutna_stanja):
    trenutna_stanja = sorted(trenutna_stanja)
    konacni = ""
    for stanje in trenutna_stanja:
        konacni += stanje + ","

    konacni = konacni[:-1] + "|"

    if konacni == "|":
        konacni = "#|"

    return konacni
