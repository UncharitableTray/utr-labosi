from metode import *

def print_sve(nfa):
    eprint("Ulazni nizovi: ")
    eprint(nfa.ulazni_nizovi)
    eprint("Stanja: ")
    stanja = ""
    for stanje in nfa.stanja:
        stanja += stanje + ","
    eprint(stanja)
    eprint("Abeceda: ")
    eprint(nfa.abeceda)
    eprint("Prijelazi: ")
    eprint(nfa.prijelazi)
    eprint("prihvatljiva_stanja: ")
    eprint(nfa.prihvatljiva_stanja)
    eprint("Pocetno stanje: ")
    eprint(nfa.pocetno_stanje)

nfa = zapocni()
print_sve(nfa)

for niz in nfa.ulazni_nizovi:

    konacni_niz = ""
    trenutna_stanja = set()
    ctrenutna_stanja.add(nfa.pocetno_stanje)

    for znak in niz:
        e_stanja = iteriraj_po_epsilon(nfa, trenutna_stanja)
        for stanje in e_stanja:
            trenutna_stanja.add(stanje)

        konacni_niz += ispis_stanja(trenutna_stanja)

        nova_stanja = mijenjaj_stanja(nfa, trenutna_stanja, znak)
        trenutna_stanja = nova_stanja

    #e-prijelaz nakon konacnog prijelaza
    e_stanja = iteriraj_po_epsilon(nfa, trenutna_stanja)
    for stanje in e_stanja:
        trenutna_stanja.add(stanje)

    konacni_niz += ispis_stanja(trenutna_stanja)
    print(konacni_niz[:-1])
