class eNFA:
    ulazni_nizovi = None
    stanja = None
    abeceda = None
    prijelazi = None
    prihvatljiva_stanja = None
    pocetno_stanje = None

    def __init__(self, ulazni, Q, sigma, delta, F, q0):
        self.stvori_ulazni(ulazni)
        self.stanja = tuple(Q)
        self.abeceda = tuple(sigma)
        self.prihvatljiva_stanja = tuple(F)
        self.pocetno_stanje = q0       #jer je pocetno jel
        self.stvori_dictionary(delta)

    def stvori_dictionary(self, delta):
        dicc = {}
        
        for prijelaz in delta:
            parts = prijelaz.split("->")
            if "\n" in parts[1]:
                parts[1] = parts[1][:-1]
            parts[1] = parts[1].split(",")

            if parts[0] not in dicc:
                dicc[parts[0]] = parts[1]
            else:
                parts[1] += dicc[parts[0]]
                dicc[parts[0]] = parts[1]

        #print(dicc)        #za ispis cijelog rjecnika
        self.prijelazi = dicc
        return

    def stvori_ulazni(self, ulazni):
        #print(ulazni[0])   #za ispis cijelog ulaznog niza
        #lista = ulazni[0].split(",")
        self.ulazni_nizovi = ulazni

        return