#!/usr/bin/python

"""
Korisetnje:
    ./gen.py [brStanja] [brSimbola] [brZadovljavajucihStanja]
"""

import sys
import string
import random

N = 100
K = 10000
VALID = 4

if len(sys.argv) > 1: N = int(sys.argv[1])
if len(sys.argv) > 2: K = int(sys.argv[2])
if len(sys.argv) > 3: VALID = int(sys.argv[3])

if VALID > N: VALID = N

charset = string.ascii_letters + string.digits

def rand_word():
    return ''.join([random.choice(charset) for _ in range(random.randint(1, 4))])

def rand_set_of_words():
    ret = list(set([rand_word() for _ in range(N)]))
    ret.sort()
    return ret

states = rand_set_of_words()
symbols = rand_set_of_words()

def rand_test():
    return ','.join([random.choice(symbols) for _ in range(random.randint(5, 50))])



transitions = {}

for st in states:
    transitions[(st, '$')] = []
    for sy in symbols:
        transitions[(st, sy)] = []

for _ in range(random.randint(K/10, K)):
    st = random.choice(states)
    sy = random.choice(symbols)
    nst = random.choice(states)
    if st == nst: continue
    transitions[(st, sy)] += [nst]

# jos dodaj par epsilona
for _ in range(random.randint(K/1000, K/50)):
    st = random.choice(states)
    nst = random.choice(states)
    if st == nst: continue
    transitions[(st, '$')] += [nst]

for st in states:
    for sy in symbols:
        transitions[(st, sy)].sort()
        transitions[(st, sy)] = list(set(transitions[st, sy]))

print '|'.join([rand_test() for _ in range(10)])
print ','.join(states)
print ','.join(symbols)
print ','.join(states[0:VALID]) # kao prihvatljiva stanja
print random.choice(states)

for st in states:
    trans = transitions[(st, '$')]
    if trans:
        print st + ',$->' + ','.join(trans) 
    for sy in symbols:
        trans = transitions[(st, sy)]
        if not trans: continue
        print st + ',' + sy + '->' + ','.join(trans)
