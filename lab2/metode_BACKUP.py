from __future__ import print_function
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def stvori():
    automat = DFA()
    automat.Q = sorted(tuple(obradi(input(), ",")))
    automat.sigma = sorted(tuple(obradi(input(), ",")))
    automat.accept = sorted(tuple(obradi(input(), ",")))
    automat.pocetno = input()

    lista_prijelaza = []
    for line in sys.stdin:
        lista_prijelaza.append(line)

    automat.delta = stvori_prijelaze(lista_prijelaza)

    #eprint("Stanja: " + str(automat.Q))
    #eprint("Abeceda: " + str(automat.sigma))
    #eprint("Prihvatljiva: " + str(automat.accept))
    #eprint("Pocetno: " + str(automat.pocetno))
    #eprint("Prijelazi: " + str(automat.delta))

    return automat

def obradi(string, delimiter):
    lista_stringova = string.split(delimiter)
    skup = set(lista_stringova)
    return skup

def stvori_prijelaze(delta):
    dicc = {}

    for prijelaz in delta:
        parts = prijelaz.split("->")
        if "\n" in parts[1]:
            parts[1] = parts[1][:-1]
            parts[1] = parts[1].split(",")

        if parts[0] not in dicc:
            dicc[parts[0]] = parts[1]
        else:
            parts[1] += dicc[parts[0]]
            dicc[parts[0]] = parts[1]

    return dicc

class DFA:
    Q = None
    sigma = None
    delta = None
    pocetno = None
    accept = None

    dicc = None
    reverse_dicc = None
    matrica = None
    parovi = None
    lista_parova = None
    tranzicije_numericke = {}

    def __init__(self):
        Q = []
        sigma = []
        delta = {}
        pocetno = None
        accept = []

        dicc = {}
        reverse_dicc = {}
        matrica = {}
        parovi = []
        lista_parova = []
        tranzicije_numericke = {}

    def eliminiraj_nedohvatna(self):
        dohvatljiva_stanja = []
        dohvatljiva_stanja.append(self.pocetno)
        doslo_do_promjene = True

        while doslo_do_promjene:
            doslo_do_promjene = False

            kandidati = []
            for stanje in dohvatljiva_stanja:
                for znak in self.sigma:
                    kljuc = self.spoji(stanje, znak)
                    novi = (self.delta[kljuc])
                    novi = str(novi).split("'")
                    kandidati.append(novi[1])

            for kandidat in kandidati:
                if kandidat not in dohvatljiva_stanja:
                    dohvatljiva_stanja.append(kandidat)
                    doslo_do_promjene = True

        self.Q = sorted(tuple(dohvatljiva_stanja))

        temp = []
        for stanje in self.accept:
            if stanje in self.Q:
                temp.append(stanje)
        self.accept = temp
        #eprint("Nova dohvatljiva su: " + str(self.Q))
        #eprint("Nova prihvatljiva su: " + str(self.accept))

        self.eliminiraj_prijelaze()

        return

    def eliminiraj_prijelaze(self):
        kljucevi = self.delta.keys()
        za_izbaciti = []

        for key in kljucevi:
            lista = key.split(",")
            if lista[0] not in self.Q:
                za_izbaciti.append(key)

        for kljuc in za_izbaciti:
            self.delta.pop(kljuc, None)

        eprint("Nova funkcija prijelaza je: " + str(self.delta))
        return

    def eliminiraj_istovjetna(self):
        self.inicijaliziraj_strukture()
        self.oznaci_pocetne()

        eprint("Dictionary je: " + str(self.dicc))
        eprint("Reverse dictionary je: " + str(self.reverse_dicc))
        #eprint("Parovi su: " + str(self.parovi))
        #eprint("Lista parova: " + str(self.lista_parova))
        eprint("Matrica je: " + str(self.matrica))

        self.glavna_petlja()

        return

    def glavna_petlja(self):
        for par in self.parovi:
            if self.pronadi_oznacen_prijelaz(par) is True:
                eprint("Najs! " + str(par))
                #self.oznaci_rekurzivno()
            else:
                self.dodaj_u_pridruzene(par)

        return

    def pronadi_oznacen_prijelaz(self, par):
        for znak in self.sigma:
            slajs = par.split(",")
            #eprint(slajs)

            p = str(self.reverse_dicc[int(slajs[0])]) + "," + znak
            q = str(self.reverse_dicc[int(slajs[1])]) + "," + znak
            #eprint(p)
            #eprint(q)

            dio_p = self.delta[p]
            dio_q = self.delta[q]

            #eprint(dio_p)
            #eprint(dio_q)

            if dio_p[0] != dio_q[0]:
                manji = min(dio_p[0], dio_q[0])
                veci = max(dio_p[0], dio_q[0])

                #eprint(manji)
                #eprint(veci)

                spoji = str(self.dicc[manji]) + "," + str(self.dicc[veci])

                eprint(spoji)

                if self.matrica[spoji] is True:
                    return True

        return False

    def dodaj_u_pridruzene(self, par):
        stanja = par.split(",")
        trenutno_p = stanja[0]
        trenutno_q = stanja[1]

        for znak in self.sigma:
            eprint("Par koji dodajem za " + znak + ": " + str(par))
            

    def oznaci_rekurzivno(self):
        return False

    def oznaci_pocetne(self):

        for prihvatljivo in self.accept:
            for stanje in self.Q:
                if stanje not in self.accept:
                    self.postavi(prihvatljivo, stanje)

        for stanje in self.Q:
            for drugo in self.Q:

                kljuc_prvi = self.dicc[stanje]
                kljuc_drugi = self.dicc[drugo]

                if kljuc_prvi == kljuc_drugi:
                    continue

                if kljuc_prvi not in self.accept and kljuc_drugi in self.accept:
                    self.postavi(kljuc_prvi, kljuc_drugi)
        return

    def postavi(self, kljuc_prvi, kljuc_drugi):
        minkljuc = min(kljuc_prvi, kljuc_drugi)
        maxkljuc = max(kljuc_prvi, kljuc_drugi)

        min_prvi = self.dicc[minkljuc]
        max_drugi = self.dicc[maxkljuc]

        kljuc_matrice = self.spoji(min_prvi, max_drugi)
        self.matrica[kljuc_matrice] = True

        return

    def stvori_listu_parova(self):
        parovi = {}
        kljucevi = self.matrica.keys()
        for kljuc in kljucevi:
            parovi[kljuc] = []

        return parovi

    def stvori_dicc(self):
        dicc = {}
        ctr = 0

        for stanje in self.Q:
            dicc[stanje] = ctr
            ctr += 1

        self.dicc = dicc
        return

    def stvori_reverse_dicc(self):
        inverted_dicc = {v: k for k, v in self.dicc.items()}
        self.reverse_dicc = inverted_dicc

        return

    def stvori_praznu_matricu(self):
        matrica_u_rjecniku = {}

        for stanje in self.Q:
            for drugo_stanje in self.Q:
                if stanje is drugo_stanje:
                    continue

                manji = min(str(self.dicc[stanje]), str(self.dicc[drugo_stanje]))
                veci = max(str(self.dicc[stanje]), str(self.dicc[drugo_stanje]))
                kljuc = self.spoji(manji, veci)
                matrica_u_rjecniku[kljuc] = False

        #eprint(str(matrica_u_rjecniku))
        self.matrica = matrica_u_rjecniku
        return

    def stvori_tranzicije_numericke(self):
        for kljuc in self.delta:
            parcijala = kljuc.split(",")

            rezultat_tranzicije = self.delta[kljuc]
            broj_rezultata = self.dicc[rezultat_tranzicije[0]]

            broj_stanja = self.dicc[parcijala[0]]
            novi_kljuc = str(broj_stanja) + "," + parcijala[1]
            self.tranzicije_numericke[novi_kljuc] = broj_rezultata

        eprint("Numericki dictionary je: " + str(self.tranzicije_numericke))

        return

    def inicijaliziraj_strukture(self):
        self.stvori_dicc()
        self.stvori_reverse_dicc()
        self.stvori_praznu_matricu()
        self.parovi = self.matrica.keys()                         #popis svih parova
        self.lista_parova = self.stvori_listu_parova()    #lista parova za algo iz knjige
        self.stvori_tranzicije_numericke()

        return

    def spoji(self, prvi, drugi):
        rez = str(prvi) + "," + str(drugi)

        return rez
