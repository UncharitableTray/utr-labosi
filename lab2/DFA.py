from __future__ import print_function
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class DFA:
    Q = []
    sigma = []
    delta = {}
    pocetno = None
    accept = []

    def __init__(self):
        Q = []
        sigma = []
        delta = {}
        pocetno = None
        accept = []

#==================================================
#             ELIMINACIJA NEDOHVATNIH
#==================================================

    def eliminiraj_nedohvatna(self):
        pocetno = self.pocetno
        

        self.Q = sorted(tuple(dohvatljiva_stanja))

        temp = []
        for stanje in self.accept:
            if stanje in self.Q:
                temp.append(stanje)
        self.accept = temp

        self.eliminiraj_prijelaze()

        eprint("Nova dohvatljiva su: " + str(self.Q))
        eprint("Nova prihvatljiva su: " + str(self.accept))
        eprint("Nova funkcija prijelaza je: " + str(self.delta))

        return

    def eliminiraj_prijelaze(self):
        kljucevi = self.delta.keys()
        za_izbaciti = []

        for key in kljucevi:
            lista = key.split(",")
            if lista[0] not in self.Q:
                za_izbaciti.append(key)

        for kljuc in za_izbaciti:
            self.delta.pop(kljuc, None)

        return

#==================================================
#             ELIMINACIJA ISTOVJETNIH
#==================================================

    def init_provjere(self, lista):
        i, j = 0, 0

        for i in range(len(self.Q)-1):
            while (j < len(self.Q)):
                for k in range(len(lista)):
                    print(lista[k])
                lista.append(self.Q[i])
                lista.append(self.Q[j])

                for l in range(len(lista)):
                    print(lista[l])
            if self.provjeri_istovjetnost(self.Q[i], self.Q[j], lista):
                if self.Q[i] in self.accept:
                    self.accept.remove(self.Q[j])
                self.Q.remove(self.Q[j])
                j -= 1
            j += 1

            while len(lista) > 0:
                lista.remove(lista[0])

            for z in lista:
                print(z)
            for o in self.Q:
                print(o)

        return

    def provjeri_istovjetnost(self, p, q, lista):

        if (((p in self.accept and q not in self.accept))
                or ((p not in self.accept) and (q in self.accept))):
            return False

        for znak in self.sigma:
            novi_p = self.delta[str(p) + "," + znak]
            novi_q = self.delta[str(q) + "," + znak]

            if ((novi_p is None and novi_q is not None)
                    or (novi_p is not None) and (novi_q is None)):
                return False

            if novi_p is not None and novi_q is not None:
                lista.append(novi_p)
                lista.append(novi_q)

        if p == q and novi_p == novi_q:
            return False
        elif p in lista:
            lista.remove(p)
        if q in lista:
            lista.remove(q)

        if not lista:
            return True

        if self.provjeri_istovjetnost(lista[0], lista[1], lista) is True:
            return True
        else:
            return False

#==================================================
#                  UTILITY METODE
#==================================================
