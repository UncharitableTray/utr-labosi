from __future__ import print_function
import sys
import re

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class DFA:
    stanja = []
    sigma = []
    delta = {}
    pocetno = None
    accept = []
    lista = []

    def __init__(self, stanja, sigma, delta, pocetno, accept, lista):
        stanja = stanja
        sigma = sigma
        delta = delta
        pocetno = pocetno
        accept = accept
        lista = lista

